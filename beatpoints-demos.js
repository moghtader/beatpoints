var i = 0;
while(i < 5) {
	i++;
}
console.log(i);


//mergesort
function mergeSort (arr) {    
    if (arr.length < 2) {
    	return arr;
    }
    var mid = Math.floor(arr.length /2);
    var subLeft = mergeSort(arr.slice(0,mid));
    var subRight = mergeSort(arr.slice(mid));
    
    return merge(subLeft, subRight);
}

function merge (a,b) {
    var result = [];
    while (a.length >0 && b.length >0)
        result.push(a[0] < b[0]? a.shift() : b.shift());
    return result.concat(a.length? a : b);
}

var test = [1,2,9,3,2,5,14,0];
console.log(mergeSort(test)); // -> [0, 1, 2, 2, 3, 5, 9, 14]



//server demo, could use timing of some sort in the future
function serveIndex() {
	console.log("Index");
}

function serveAbout() {
	console.log("About");
}

function serveLogin() {
	console.log("Login");
}

var requestPos = [1, 2, 0];
for(var iter = 0; iter < 20; iter++) {
	var req = Math.floor((Math.random() * requestPos.length));
	console.log(req);
	switch(requestPos[req]) {
		case 0:
			serveIndex();
			break;
		case 1:
			serveAbout();
			break;
		case 2:
			serveLogin();
			break;
	}
}


//tracing through code