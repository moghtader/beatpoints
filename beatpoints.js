$(document).ready(function() {


	// DOM element where the Timeline will be attached
	var container = document.getElementById('visualization');

	// Create a DataSet (allows two way data-binding)
	var runData = [];
	
	var notes = [];
    var speed = 1;
    $( "#speed" ).slider({
      value:200,
      min: 0,
      max: 400,
      step: 50,
      slide: function( event, ui ) {
        speed = ui.value/200;
      	console.log("speed changed" + speed);
      }});

	$("textarea#code").elastic(); //automatically resizes the textbox

	var newlineCount = 0;
	$('textarea#code').keyup(function(e) {
			
		var count =  $(this).val().split(/\r\n|\r|\n/).length;
  		while(count > newlineCount) {
	        $("#lines").append('<tr class="gutter"><td class="beatpoint clear" style="font-size:11px">s</td></tr>');
	        newlineCount++;
  		}

  		while(count < newlineCount) {
  			$("#lines").find("tr:last").remove();
  			newlineCount--;
  		}
	    
	});

	$(document).on("contextmenu", ".beatpoint", function(e) {
		e.preventDefault();
		var t = $(this).text();
		switch(t) {
			case "s":
				$(this).text("e");
				break;
			case "e":
				$(this).text("s");
				break;
			default:
				$(this).text("s");
		}
	});

	$(document).on("click", ".red", function(e) {
		$(this).removeClass("red");
		$(this).addClass("blue");
	});

	$(document).on("click", ".blue", function(e) {
		$(this).removeClass("blue");
		$(this).addClass("green");
	});

	$(document).on("click", ".green", function(e) {
		$(this).removeClass("green");
		$(this).addClass("gray");
	});

	$(document).on("click", ".gray", function(e) {
		$(this).removeClass("gray");
		$(this).addClass("brown");
	});

	$(document).on("click", ".brown", function(e) {
		$(this).removeClass("brown");
		$(this).addClass("clear");
	})

	$(document).on("click", ".clear", function(e) {
		$(this).removeClass("clear");
		$(this).addClass("red");
	});

	function playMidi() {
		var delay = 0; // play one note every quarter second
		var note = 50; // the MIDI note
		var velocity = 127; // how hard the note hits
		// play the note
		for(var i = 0; i < notes.length; i++) {
			console.log("Playing " + notes[i]);
			if(notes[i] >= 0) {
				MIDI.setVolume(0, 127);
				MIDI.noteOn(0, notes[i], velocity, speed*i/4.0);
				MIDI.noteOff(0, (notes[i], i/4.0 + .3)*speed);
			} else {
				MIDI.setVolume(0, 127);
				MIDI.noteOn(0, 1, 0, speed*i/4.0);
				MIDI.noteOff(0, 1, (i/4.0 + .3)*speed);
			}
		}
	}

	MIDI.loadPlugin({
			soundfontUrl: "midi/soundfont/",
			instrument: "acoustic_grand_piano",
			onprogress: function(state, progress) {
				console.log(state, progress);
			}
	});
	function runMidi() {
		playMidi();
	}

	
	
	$('#replay').click(runMidi);

	$("#run").click(function(){
		var code = $("textarea#code").val();
		$("#visualization").empty();
		runData = [];
		notes = [];
		var myTableArray = [];
		$("table#lines tr").each(function() {
	    	var arrayOfThisRow = [];
	   		var tableData = $(this).find('td');
		    if (tableData.length > 0) {
		        tableData.each(function() { 
		        	var note = null;
		        	if($(this).hasClass("red"))
		        		note = 1;
		        	else if($(this).hasClass("blue"))
		        		note = 2;
		        	else if($(this).hasClass("green"))
		        		note = 3;
		        	else if($(this).hasClass("gray"))
		        		note = 4;
		        	else if($(this).hasClass("brown"))
		        		note = 5;

		        	
		        	if(note != null)
		        		myTableArray.push({"mode": $(this).text(), "note": note});
		        	else
		        		myTableArray.push(null);
		        });
		    }
		});
		
		var lines = code.split(/\r?\n/);

		for(var i = 0; i < myTableArray.length; i++) {
			if(myTableArray[i] != null) {
				if(myTableArray[i].mode == "s")
					lines[i] = "beatpoint("+myTableArray[i].note+");\n" + lines[i];
				else if(myTableArray[i].mode == "e")
					lines[i] += "beatpoint("+myTableArray[i].note+");\n";
			}
		}

		var execCode = lines.join("\n");
		var res = eval(execCode);
		var items = new vis.DataSet(runData);

		// Configuration for the Timeline
		var options = {};

		// Create a Timeline
		var timeline = new vis.Timeline(container, items, options);

		runMidi();
	});

	
	var id = 0;
	var lastTime = 0;
	function beatpoint(note) {
		var color = "#518CDB";
		var midiNote = 60;
		switch(note) {
			case 2:
				color = "#E5933F";
				midiNote = 64;
				break;
			case 3:
				color = "#4AE583";
				midiNote = 67;
				break;
			case 4:
				color = "#232323";
				midiNote = -1;
				break;
			case 5:
				color = "#5C4033";
				midiNote = 66;
				break;
		}
		if(Date.now() <= lastTime) {
			runData.push({id: id, type: "box", style: "background-color: " + color + ";", group: midiNote, content: '', start: lastTime + 1});
			lastTime = lastTime + 1;
		} else {
			runData.push({id: id, type: "box", style: "background-color: " + color + ";", group: midiNote, content: '', start: Date.now()});
			lastTime = Date.now();
		}
		id++;
		notes.push(midiNote);
	}
});
